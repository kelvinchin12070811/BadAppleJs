import * as blessed from 'blessed'
import parser from 'fast-xml-parser'
import fs from 'fs'

const fps = 30
const frameTime = 1000 / fps
const showFps = true // Display FPS counter

console.log('Loading frames...')

const framesReader = fs.readFileSync('./resources/frames.xml', 'utf-8')
const frames = parser.parse(framesReader)

let frameCount = 0
let lastFrameCount = 0
let frameIdx = 0

const screen = blessed.screen({
    smartCSR: true
})

const box = blessed.box({
    top: 'top',
    left: 'left',
    width: '100%',
    height: '100%',
    content: "hello world"
})

screen.append(box)

if (frames == null)
{
    console.log('Unable to load frames')
    process.exit()
}

const draw = () => {
    if (frameIdx >= frames.frames.frame.length)
        process.exit()

    let rdBuffer = ""

    frames.frames.frame[frameIdx].line.forEach((itr: string) => {
        rdBuffer += itr + '\n'
    });

    if (showFps)
    {
        const fpsStr = `${lastFrameCount} FPS  `
        rdBuffer =  fpsStr + rdBuffer.substr(fpsStr.length, rdBuffer.length)
    }

    box.content = rdBuffer

    screen.render()
    frameCount++
    frameIdx++
}

const frameCounter = () => {
    lastFrameCount = frameCount
    frameCount = 0
}

setInterval(draw, frameTime)
setInterval(frameCounter, 1000)